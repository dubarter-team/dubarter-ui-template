// Gulpfile
'use strict';
var
    gulp = require('gulp'),
    image = require('gulp-image'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');    
    // autoprefixer = require('gulp-autoprefixer'),    
    // postcss = require('gulp-postcss');


livereload({ start: true })

//compressed image size
gulp.task('image-compressed-task', function (done) {
    // do stuff
    gulp.src('images/*')
        .pipe(image())
        .pipe(gulp.dest('assets/img'))
        .pipe(livereload());
    done();
});

// uglify js
gulp.task('js', function (done) {
    gulp.src('scripts/*.js')
        // .pipe(plumber())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('assets/js'))
        .pipe(livereload())
    done();
});

//clean css
gulp.task('css', function (done) {
    gulp.src('styles/*.css')
        // .pipe(plumber())
        // .pipe(postcss(autoprefixer({
        //     browsers:['last 10 versions']
        // })))
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('assets/css'))
        .pipe(livereload());
    done();
})

gulp.task('html', function (done) {
    gulp.src('../*.html')
        .pipe(livereload())
    done()
})
// watch
gulp.task('watch', function (done) {
    livereload.listen();
    gulp.watch('*.html', gulp.series('html'));
    gulp.watch('scripts/*.js', gulp.series('js'));
    gulp.watch('styles/*.css', gulp.series('css'));
    gulp.watch('styles/*.scss', gulp.series('sass'));    
    gulp.watch('images/*', gulp.series('image-compressed-task'));
    done()
})

gulp.task('sass', function (done) {
   return gulp.src('styles/*.scss')
   .pipe(sourcemaps.init())   
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())    
    .pipe(gulp.dest('assets/css'));
    done()
});

gulp.task('default', gulp.series('image-compressed-task', 'js', 'css', 'watch', 'html','sass', function (done) {
    // do more stuff
    done();
}));