document.addEventListener('DOMContentLoaded', function () {
    $('.nav-link.dropdown-toggle').on('click', function () {
        $(this)
            .find('[data-fa-i2svg]')
            .toggleClass('fa-angle-up')
            .toggleClass('fa-angle-down');
    });
});
$(function () {
    'use stract'
    $(".flex-slide").each(function () {
        $(this).hover(function () {
            $('.flex-slide').css({
                width: '10%'
            });
            $('.header-box h3').css({
                opacity: '0'
            });

            $(this).css({
                width: '60%'
            });
            $(this).find('.header-box h3').css({
                opacity: '1'
            });
            $(this).find('.header-box').css({
                opacity: '1'
            });
            $(this).find('.slide-details').css({
                opacity: '1'
            });
        }, function () {
            $('.flex-slide').css({
                width: '20%'
            });
            $(this).find('.flex-about').css({
                opacity: '0'
            });
            $('.header-box h3').css({
                opacity: '1'
            });
            $('.slide-details').css({
                opacity: '0'
            });
        })
    });
    $(document).on("click", ".scroll-section", function (e) {

        e.preventDefault();

        var id = $(this).attr("href"),
            topSpace = 50;

        $('html, body').animate({
            scrollTop: $(id).offset().top - topSpace
        }, 800);

        window.location.hash = id;
    });
    var searchInput = $(".search-input");
    var cityInput = $(".city-input");
    searchInput.typeahead({
        source: [{
            id: "1",
            name: "عقاربارتر"
        },
            {
                id: "2",
                name: "فودي بارتر"
            },
            {
                id: "3",
                name: "ستايل بارتر"
            },
            {
                id: "4",
                name: "اوتوبارتر"
            },
            {
                id: "5",
                name: "مبوبه بارتر"
            },
            {
                id: "6",
                name: "مقالات بارتر"
            },
        ],
        autoSelect: true
    });
    var city = [{
        id: "1",
        name: "القاهرة"
    },
        {
            id: "2",
            name: "الأسكندريه"
        },
        {
            id: "3",
            name: "المنصوره"
        },
        {
            id: "4",
            name: "المنوفيه"
        },
    ];

    var carModel = [{
        id: 0,
        text: 'سبورتاج'
    },
        {
            id: 1,
            text: 'هاتش باج'
        },
        {
            id: 2,
            text: 'سانديروا'
        },
        {
            id: 3,
            text: 'لوجان'
        }
    ];

    var purchaseMethod = [{
        id: 0,
        text: 'بيع'
    },
        {
            id: 1,
            text: 'شراء'

        }
    ]
    var propertyType = [{
        id: 0,
        text: 'فيلا'
    },
        {
            id: 0,
            text: 'شقه'
        }, {
            id: 0,
            text: 'اراضي'
        }
    ];

    var cityID = [{
        id: "1",
        text: "القاهرة"
    },
        {
            id: "2",
            text: "الأسكندريه"
        },
        {
            id: "3",
            text: "المنصوره"
        },
        {
            id: "4",
            text: "المنوفيه"
        },
    ];

    cityInput.typeahead({
        source: city,
        autoSelect: true
    });

    $.scrolltop({
        template: '<i class="fa fa-chevron-up"></i>',
        class: 'custom-scrolltop'
    });

    $("h4").fitText(1.2, {
        minFontSize: '18px',
        maxFontSize: '24px'
    });

    $(".owl-prev").addClass('disabled');


    $('#videos .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $('#aqura-barter .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $('#auto-barter .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $('#cats-page .new-car .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $('#cats-page .old-car .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $('#style-barter .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
    });

    $('#blog-barter .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
    });

    $('#artical-barter .owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });
    $(".js-example-data-array.car-make").select2({
        data: [],
        dir: "rtl"
    })
    $(".js-example-data-array.car-model").select2({
        data: [],
        dir: "rtl"
    })

    $(".js-example-data-array.car-location").select2({
        data: [],
        dir: "rtl"
    })

    $(".js-example-data-array.city").select2({
        data: [],
        dir: "rtl"
    })
    $(".js-example-data-array.purchase-method").select2({
        data: [],
        dir: "rtl"
    })
    $(".js-example-data-array.property-type").select2({
        data: [],
        dir: "rtl"
    })
    $(".js-example-data-array.best-offer").select2({
        // data: propertyType,
        dir: "rtl"
    })

    $(".js-example-data-array.department").select2({
        // data: propertyType,
        dir: "rtl"
    })
    // $(".fa-heart").click(function(){
    //     this.removeClass('far').addClass('fas');
    // })
    // $( ".fa-heart").click(function() {
    //     if ($(this).hasClass('.far')){
    //         $(this).removeClass('far').addClass('fas');
    //     }

    //   });
    $('.js-example-data-array').select2({
        dir:"rtl"
    })

   var loginForm= $('.form-container').height();
   var space=loginForm + 16;
   $(".login-bg").css("height", space);
});
    
