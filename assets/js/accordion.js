(function () {


    $(".flex-slide").each(function () {
        $(this).click(function () {
            $(this).find(".gray-overlay").first().css("opacity", 0);

            $('.flex-slide').css({
                width: '10%'
            });
            $('.header-box h3').css({
                opacity: '0'
            });

            $(this).css({
                width: '60%'
            });
            $(this).find('.header-box h3').css({
                opacity: '1'
            });
            $(this).find('.header-box').css({
                opacity: '1'
            });
            $(this).find('.slide-details').css({
                opacity: '1'
            });
        }, function () {

            $(this).find(".gray-overlay").first().css("opacity", 0.4);

            $('.flex-slide').css({
                width: '20%'
            });
            $(this).find('.flex-about').css({
                opacity: '0'
            });
            $('.header-box h3').css({
                opacity: '1'
            });
            $('.slide-details').css({
                opacity: '0'
            });
        })
    });

})();

